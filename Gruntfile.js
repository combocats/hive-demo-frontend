'use strict';

module.exports = function (grunt) {
    var bowerDir = 'bower_components';

    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        ngtemplates: {
            app: {
                options: {
                    base: 'app/templates',
                    concat: 'app'
                },
                src: [
                    'app/templates/**/*.html'
                ],
                dest: 'compiled/js/templates.js'
            }
        },
        html2js: {
            widgets : [ 'app/js/lib/ui/templates/**/*.html' ]
        },
        concat: {
            app: {
                src: [
                    bowerDir + '/lodash/dist/lodash.min.js',
                    bowerDir + '/angular/angular.js',
                    bowerDir + '/angular-route-segment/build/angular-route-segment.js',
                    bowerDir + '/angular-ui-bootstrap3/ui-bootstrap-tpls.js',
                    'app/config/<%= AppConfig.file %>',
                    'app/js/**/*.js'
                ],
                dest: 'build/app.js'
            },
            css: {
                src: [
                    bowerDir + '/bootstrap/dist/css/bootstrap.css',
                    'app/css/app.css'
                ],
                dest: 'build/css/app.css'
            }
        },
        index: {
            default: {
                base: "http://localhost:3000/"
            },
            prod: {
                base: "http://95.85.6.31/"
            }
        },
        copy: {
            fonts: {
                expand: true,
                cwd:  bowerDir + '/bootstrap/dist/fonts/',
                src: '*',
                dest: 'build/fonts/'
            }
        }
    });

    var TPL = 'angular.module("<%=file%>", []).run(function($templateCache) {\n'
            + '  $templateCache.put("<%=file%>",\n    "<%=content%>");\n'
            + '});\n';

    var escapeContent = function(content) {
        return content.replace(/"/g, '\\"').replace(/\r?\n/g, '" +\n    "');
    };

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-angular-templates');

    grunt.registerMultiTask('html2js', 'Generate js version of html templates for widgets.',
        function() {
            var files = grunt._watch_changed_files || grunt.file.expand(this.data);

            files.forEach(function(file) {
                grunt.file.write(file + '.js', grunt.template.process(TPL, {
                    data: {
                        file : file.replace(/^app\//,''),
                        content : escapeContent(grunt.file.read(file))
                        }
                    }));
            });
        });

    grunt.registerMultiTask('index', 'Create index.html', function() {
        var tpl = grunt.file.read('app/index.html');

        grunt.file.write('build/index.html', grunt.template.process(tpl, {
            data: {
                base_url: this.data.base
            }
        }));
    });

    grunt.registerTask('default', function() {
        grunt.config.set('AppConfig.file', 'dev.js');
        grunt.task.run(['ngtemplates', 'html2js:widgets', 'concat', 'index:default', 'copy']);
    });
    grunt.registerTask('prod', function() {
        grunt.config.set('AppConfig.file', 'prod.js');
        grunt.task.run(['ngtemplates', 'html2js:widgets', 'concat', 'index:prod', 'copy']);
    });
};