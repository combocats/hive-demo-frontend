'use strict';

angular.module('app', [
        'route-segment',
        'view-segment',
        'app.pages',
        'ui.bootstrap',
        'config'
    ])

    .config([
        '$routeSegmentProvider',
        '$locationProvider',
        'ApiProvider',
        'API_URL',
    function($routeSegmentProvider, $locationProvider, ApiProvider, API_URL){
        $routeSegmentProvider.options.autoLoadTemplates = true;
        $routeSegmentProvider.options.strictMode = true;

        ApiProvider.options.apiUrl = API_URL;

        $locationProvider.html5Mode(true);
    }])

    .run(function($rootScope, $routeSegment){
        $rootScope.routeSegment = $routeSegment;
    });