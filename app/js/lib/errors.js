'use strict';

angular.module('app.lib.errors', [])
    .factory('Errors', function() {
        var error = {};
        var Error = {};

        Error.set = function(err) {
            console.log(" set error: ", err);
            error = err;
        };

        Error.get = function() {
            console.log(" get error: ", error);
            return error;
        };

        return Error;
    })
    .controller('ErrorsCtrl', ['$scope', function($scope) {
        console.log("ErrorsCtrl running");
    }]);