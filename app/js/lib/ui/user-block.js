angular.module('app.lib.ui.userBlock', [
        'js/lib/ui/templates/user-block.html',
        'app.dal.entities.user'
    ])

    .directive('cwUserBlock', [function() {
        return {
            restrict: 'E',
            templateUrl: 'js/lib/ui/templates/user-block.html',
            replace: true,
            controller: 'UserBlockCtrl'
        }
    }])
    .controller('UserBlockCtrl', ['$scope', 'User', function($scope, User) {
        $scope.user = User.me();

        $scope.logout = function() {
            User.logout().then(function() {
                window.location.replace('/');
            });
        };
    }]);