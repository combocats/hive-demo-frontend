angular.module("js/lib/ui/templates/static-control.html", []).run(function($templateCache) {
  $templateCache.put("js/lib/ui/templates/static-control.html",
    "<div class=\"form-group\">" +
    "    <label class=\"col-sm-2 control-label\">{{ label }}</label>" +
    "    <div class=\"col-sm-6\">" +
    "        <p class=\"form-control-static\">{{ model }}</p>" +
    "    </div>" +
    "</div>");
});
