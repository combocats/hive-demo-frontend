angular.module('app.lib.ui.staticControl', ['js/lib/ui/templates/static-control.html'])

    .directive('cwStaticControl', [function() {
        return {
            restrict: 'E',
            templateUrl: 'js/lib/ui/templates/static-control.html',
            replace: true,
            scope: {
                label: '=',
                model: '='
            }
        }
    }]);