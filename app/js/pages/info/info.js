'use strict';

angular.module('app.pages.info', [
        'route-segment',
        'view-segment',
        'app.lib.errors'
    ])
    .config(['$routeSegmentProvider', function($routeSegmentProvider) {
        $routeSegmentProvider
            .when('/emailactivated', 'emailactivated')
            .when('/emailchanged', 'emailchanged')
            .when('/linkcreated', 'pwdrecoverlinkcreated')
            .when('/pwdresetcompleted', 'pwdresetcompleted')
            .when('/404', 'notfound')
            .when('/error', 'error')
            .segment('emailactivated', {
                templateUrl: 'info/emailactivated.html'
            })
            .segment('emailchanged', {
                templateUrl: 'info/emailchanged.html'
            })
            .segment('notfound', {
                templateUrl: 'info/notfound.html'
            })
            .segment('error', {
                templateUrl: 'info/error.html',
                controller: 'ErrorsCtrl'
            })
            .segment('pwdrecoverlinkcreated', {
                templateUrl: 'info/pwdrecoverlinkcreated.html'
            })
            .segment('pwdresetcompleted', {
                templateUrl: 'info/pwdresetcompleted.html'
            });
    }])
    .controller('ErrorsCtrl', ['$scope', 'Errors', function($scope, Errors) {
        $scope.error = Errors.get();
    }]);