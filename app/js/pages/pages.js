'use strict';

angular.module('app.pages', ['app.pages.user',
                             'app.pages.auth',
                             'app.pages.info',
                             'app.pages.game',
                             'app.lib.ui.userBlock'])
    .config(['$routeSegmentProvider', function($routeSegmentProvider) {
        $routeSegmentProvider
            .when('/', 'main')
            .segment('main', {
                templateUrl: 'main.html'
            });
    }]);