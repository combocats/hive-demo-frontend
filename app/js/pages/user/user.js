'use strict';

angular.module('app.pages.user', [
        'route-segment',
        'view-segment',
        'app.lib.ui.staticControl',
        'app.pages.user.controllers'
    ])
    .config(['$routeSegmentProvider', function($routeSegmentProvider) {
        $routeSegmentProvider
            .when('/user', 'user')
            .when('/user/profile', 'user.profile')
            .when('/user/password', 'user.password')
            .when('/user/email', 'user.email')
            .segment('user', {
                templateUrl: 'user/index.html'
            })
            .within()
                .segment('profile', {
                    templateUrl: 'user/profile.html',
                    controller: 'ProfileCtrl'
                })
                .segment('password', {
                    templateUrl: 'user/change_password.html',
                    controller: 'ChangePwdCtrl'
                })
                .segment('email', {
                    templateUrl: 'user/change_email.html',
                    controller: 'ChangeEmailCtrl'
                });

    }]);