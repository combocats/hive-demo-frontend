'use strict';

angular.module('app.pages.user.controllers', ['app.dal.entities.user'])
    .controller('ProfileCtrl', ['$scope', 'User', function($scope, User) {
        $scope.user = User.me();
    }])
    .controller('ChangePwdCtrl', ['$scope', 'User', function($scope, User) {
        $scope.changePwd = function(credentials) {
            User.changePwd(credentials).then(function(data) {
                $scope.credentials = {};
            });
        };
    }])
    .controller('ChangeEmailCtrl', ['$scope', 'User', function($scope, User) {
        $scope.changeEmail = function(exchangeData) {
            User.changeEmail(exchangeData).then(function(data) {
                $scope.exchangeData = {};
            });
        };
    }]);