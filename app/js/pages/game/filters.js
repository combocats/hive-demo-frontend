'use strict';

angular.module('app.pages.game.filters', ['app.dal.entities.user'])
    .filter('playerlabel', ['User', function(User) {
        return function(uid) {
            return User.isMe(uid) ? 'You' : 'Opponent';
        }
    }])
    .filter('playername', ['User', function(User) {
        return function(player) {
            if(!player) return '';
            return User.isMe(player.uid) ? 'You' : player.name;
        }
    }]);