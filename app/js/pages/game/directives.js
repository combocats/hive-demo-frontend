'use strict';

angular.module('app.pages.game.directives', ['app.dal.entities.match'])
    .directive('cwGame', ['Match', function(Match) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'game/game.html',
            scope: {
                match: '='
            },
            link: function($scope, element, attrs) {
                $scope.makeMove = function(field) {
                    $scope.match.makeMove(field.id);
                };
            }
        }
    }])
    .directive('cwFieldItem', ['Match', function(Match) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'game/field-item.html',
            scope: {
                field: "=",
                makeMove: "&"
            },
            link: function($scope, element, attrs) {
                $scope.currentItem = Match.currentItem;
            }
        }
    }]);