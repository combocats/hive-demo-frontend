'use strict';

angular.module('app.pages.game.controllers', ['app.dal.entities.match'])
    .controller('GameProfileCtrl', ['$scope', function($scope) {
        $scope.gameProfile = {
            uid: 123,
            lvl: 10,
            exp: 100,
            score: 2,
            coins: 230,
            awards: ['master', 'niceguy'],
            name: 'Zahar A.'
        };

    }])
    .controller('GameFriendsCtrl', ['$scope', function($scope) {
        $scope.friends = [
            {name: "Max T."},
            {name: "Sam Wang"},
            {name: "Mary Frather"},
            {name: "Barak Obama"}
        ];
    }])
    .controller('GameMatchesCtrl', ['$scope', 'Match', function($scope, Match) {
        $scope.matches = Match.getAll();
    }])
    .controller('GameNewMatchCtrl', ['$scope', function($scope) {
        $scope.friends = [{name: 'Max T.', uid: 1}, {name: 'Mary Frather', uid: 2}];

        $scope.createMatch = function(match) {
            console.log("createMatch : ", match);
        };
    }])
    .controller('GameCurrentMatchCtrl', ['$scope', '$routeSegment', 'Match', function($scope, $routeSegment, Match) {
        console.log(" start current match: ", $routeSegment.$routeParams.id);
        Match.get($routeSegment.$routeParams.id).then(function(match) {
            $scope.match = match;
        });

        $scope.isWinner = function(playerIndex) {
            return playerIndex == $scope.match.winner;
        };

        $scope.getArtefactClass = function(name) {
            var artefactsToClassMap = {
                'droplet'   : 'glyphicon-tint',
                'plane'     : 'glyphicon-send',
                'cart'      : 'glyphicon-shopping-cart',
                'tree'      : 'glyphicon-tree-conifer',
                'tower'     : 'glyphicon-tower'
            };

            return artefactsToClassMap[name];
        };
    }]);