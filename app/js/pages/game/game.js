'use strict';

angular.module('app.pages.game', [
        'route-segment',
        'view-segment',
        'app.lib.ui.staticControl',
        'app.pages.game.controllers',
        'app.pages.game.filters',
        'app.pages.game.directives'
    ])
    .config(['$routeSegmentProvider', function($routeSegmentProvider) {
        $routeSegmentProvider
            .when('/game', 'game')
            .when('/game/home', 'game.home')
            .when('/game/profile', 'game.profile')
            .when('/game/friends', 'game.friends')
            .when('/game/matches', 'game.matches')
            .when('/game/startmatch', 'game.startmatch')
            .when('/game/matches/:id', 'game.currentmatch')
            .segment('game', {
                templateUrl: 'game/index.html'
            })
            .within()
                .segment('home', {
                    templateUrl: 'game/home.html'
                })
                .segment('profile', {
                    templateUrl: 'game/profile.html',
                    controller: 'GameProfileCtrl'
                })
                .segment('friends', {
                    templateUrl: 'game/friends.html',
                    controller: 'GameFriendsCtrl'
                })
                .segment('matches', {
                    templateUrl: 'game/matches.html',
                    controller: 'GameMatchesCtrl'
                })
                .segment('startmatch', {
                    templateUrl: 'game/creatematch.html',
                    controller: 'GameNewMatchCtrl'
                })
                .segment('currentmatch', {
                    templateUrl: 'game/currentmatch.html',
                    controller: 'GameCurrentMatchCtrl',
                    dependencies: ['id']
                });

    }]);