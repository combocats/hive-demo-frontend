'use strict';

angular.module('app.pages.auth.controllers', [
        'app.dal.api',
        'app.dal.entities.user'
    ])
    .controller('SignupCtrl', ['$scope', 'User', function($scope, User) {
        $scope.user = new User();

        $scope.signup = function() {
            $scope.user.create().then(function(user) {
                window.location.replace('/');
            });
        };
    }])
    .controller('LoginLocalCtrl', ['$scope', 'User', 'Api', function($scope, User, Api) {
        console.log("LoginLocalCtrl running");
        $scope.loginLocal = function(credentials) {
            User.loginLocal(credentials).then(function(data) {
                Api.setToken(data.accessToken);
                window.location.replace('/');
            });
        };
    }])
    .controller('PwdCecoverCtrl', ['$scope', 'User', '$location', function($scope, User, $location) {
        $scope.recover = function(email) {
            User.recoverPwd(email).then(function() {
                $location.path('/linkcreated');
            });
        };
    }])
    .controller('PwdResetCtrl', ['$scope', 'User', '$location', '$routeSegment', function($scope, User, $location, $routeSegment) {
        $scope.resetData = {
            link: $routeSegment.$routeParams.link
        };

        $scope.resetPwd = function(resetData) {
            User.resetPwd(resetData).then(function() {
                $location.path('/pwdresetcompleted');
            });
        };
    }]);