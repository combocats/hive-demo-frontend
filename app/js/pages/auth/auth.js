'use strict';

angular.module('app.pages.auth', [
        'route-segment',
        'view-segment',
        'app.pages.auth.controllers'
    ])
    .config(['$routeSegmentProvider', function($routeSegmentProvider) {
        $routeSegmentProvider
            .when('/login', 'login')
            .when('/signup', 'signup')
            .when('/pwdrecover', 'pwdrecover')
            .when('/recover/:link', 'pwdreset')
            .segment('login', {
                templateUrl: 'auth/login.html',
                controller: 'LoginLocalCtrl'
            })
            .segment('signup', {
                templateUrl: 'auth/signup.html',
                controller: 'SignupCtrl'
            })
            .segment('pwdrecover', {
                templateUrl: 'auth/pwdrecover.html',
                controller: 'PwdCecoverCtrl'
            })
            .segment('pwdreset', {
                templateUrl: 'auth/pwdreset.html',
                controller: 'PwdResetCtrl',
                dependencies: ['link']
            });
    }]);