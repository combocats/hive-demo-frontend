'use strict';

angular.module('app.dal.rest.user', ['app.dal.api'])

.factory('UserApi', ['$q', 'Api', function($q, Api){
    var UserApi = {};

    UserApi.me = function(){
        return Api.get('/api/users', null, {disableDefaultErrorHandler: true}).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.changePwd = function(data) {
        return Api.post('/api/change/pwd', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.changeEmail = function(data) {
        return Api.post('/api/change/email', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.create = function(data){
        return Api.post('/api/users', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.loginLocal = function(data){
        return Api.post('/api/auth/local', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.logout = function(){
        return Api.get('/api/logout').then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.recoverPwd = function(data){
        return Api.post('/api/pwdrecover', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    UserApi.resetPwd = function(data){
        return Api.post('/api/pwdreset', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    return UserApi;
}]);