'use strict';

angular.module('app.dal.rest.match', ['app.dal.api'])

.factory('MatchApi', ['$q', 'Api', function($q, Api) {
    var MatchApi = {};

    MatchApi.get = function(id) {
        return Api.get('/api/game1/matches/' + id).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    MatchApi.getAll = function() {
        return Api.get('/api/game1/matches').then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    MatchApi.update = function(id, data) {
        // data here is not full object but delta-changes!
        // we need this for traffic optimisation
        return Api.put('/api/game1/matches/' + id, data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    MatchApi.create = function(data) {
        return Api.post('/api/game1/matches/', data).then(
            function(response){
                return response.data;
            },
            function(response) {
                return $q.reject(response.data);
            }
        );
    };

    return MatchApi;
}]);