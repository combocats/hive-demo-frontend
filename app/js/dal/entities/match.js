'use strict';

angular.module('app.dal.entities.match', [
        'app.dal.entities.user',
        'app.dal.fake.match'
    ])

.factory('Match', ['$q', 'MatchFakeApi', 'User', function($q, MatchApi, User) {
    var cash = {};

    function populateFields(fields) {
        var i = 0;
        do {
            fields[i] = angular.extend({id: i}, fields[i] || {item: null});
            i++;
        } while(i < 9);
    }

    function areEqual() {
        var len = arguments.length;
        for (var i = 1; i < len; i++){
            if (arguments[i] == null || arguments[i] != arguments[i-1])
            return false;
        }
        return true;
    }

    var initObject = function(data) {
        if (cash[data.id]) {
            angular.extend(cash[data.id], new Match(data));
        } else {
            cash[data.id] = new Match(data);
        }
        return cash[data.id];
    };

    var Match = function(data) {
        angular.extend(this, data);
        populateFields(this.state.fields);
    };

    Match.get = function(id) {
        if(cash[id]) {
            return $q.when(cash[id]);
        } else {
            return MatchApi.get(id).then(function(match) {
                cash[id] = initObject(match);
                return cash[id];
            });
        }
    };

    Match.getAll = function() {
        return MatchApi.getAll().then(function(matches) {
            angular.forEach(matches, function(match) {
                return initObject(match);
            });
            return cash;
        });
    };

    Match.prototype.isMyTurn = function() {
        if(this.active) {
            var isMe =  User.isMe(this.players[this.state.turn].uid);
            return isMe;
        } else {
            return false;
        }
    };

    Match.prototype.setNextTurn = function() {
        var numberOfPlayers = this.players.length;
        if(this.state.turn == numberOfPlayers - 1) {
            this.state.turn = 0;
        } else {
            this.state.turn++;
        }
    };

    Match.prototype.randomMove = function() {
        var emptyFields = [];
        angular.forEach(this.state.fields, function(field) {
            if(field.item == null) emptyFields.push(field);
        });
        if(emptyFields.length) {
            var fieldKey = emptyFields[_.random(emptyFields.length - 1)].id;
            this.state.fields[fieldKey].item = this.players[1].item;
        }
    };

    Match.prototype.getMyItem = function() {
        return _.find(this.players, function(player) {
            return User.isMe(player.uid);
        }).item;
    };

    Match.prototype.setFieldItem = function(field) {
        if(this.state.fields[field].item == null) {
            this.state.fields[field].item = this.getMyItem();
            this.setNextTurn();
            return $q.when(this.state.fields[field]);
        } else {
            return $q.reject('ERROR: field is not empty!');
        }
    };

    Match.prototype.makeMove = function(field) {
        var self = this;
        if(self.isMyTurn()){
            return self.setFieldItem(field)
                .then(function(field) {
                    MatchApi.makeMove(self.id, field).then(function(match) {
                        initObject(match);
                    });
                    return true;
                });
        } else {
            return $q.reject(false);
        }
    };

    Match.currentItem = function(item) {
        if(item === 1) {
            return 'x';
        }
        if(item === 0) {
            return 'o';
        }
        return null;
    };

    return Match;
}]);
