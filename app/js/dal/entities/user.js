'use strict';

angular.module('app.dal.entities.user', ['app.dal.rest.user'])

.factory('User', ['$q', 'UserApi', function($q, UserApi){
    var user;

    var User = function(data) {
        angular.extend(this, data);
    };

    User.me = function() {
        if(user) {
            return $q.when(user);
        } else {
            return UserApi.me().then(function(userData){
                user = new User(userData);
                return user;
            });
        }
    };

    User.isMe = function(uidOrId) {
        return user.uid == uidOrId || user._id == uidOrId;
    };

    User.prototype.create = function() {
        var self = this;

        return UserApi.create(this).then(function(data){
            angular.extend(self, data);
            return self;
        });
    };

    User.changePwd = function(credentials) {
        return UserApi.changePwd(credentials).then(function(data){
            return data;
        });
    };

    User.changeEmail = function(exchangeData) {
        return UserApi.changeEmail(exchangeData).then(function(data){
            return data;
        });
    };

    User.recoverPwd = function(email) {
        return UserApi.recoverPwd({email: email}).then(function(data) {
            return data;
        });
    };

    User.resetPwd = function(resetData) {
        return UserApi.resetPwd(resetData).then(function(data) {
            return data;
        });
    };

    User.loginLocal = function(credentials) {
        return UserApi.loginLocal(credentials).then(function(data) {
            return data;
        });
    };

    User.logout = function() {
        return UserApi.logout();
    };

    return User;
}]);