'use strict';

angular.module('app.dal.api', ['app.lib.errors'])

.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
])

.provider('Api', function(){

    var options = this.options = {
        apiUrl: ''
    };

    this.$get = ['$http', '$q', '$rootScope', '$location', 'Errors', function($http, $q, $rootScope, $location, Errors) {

        var Api = {};

        function makeParams(params) {
            var token = Api.getToken();
            if(token) {
                params = params || {};
                return angular.extend(params, {accessToken: token});
            } else {
                return params;
            }
        }
        /**
         * Returns default error handler. If disableDefaultErrorHandler param is true
         * default error handling would not be processed.
         * @param config
         * @returns {Function}
         */
        var getErrorHandler = function(config) {
            return function(response) {
                if(config && config.disableDefaultErrorHandler) {
                    console.log("default error handler is disabled!");
                } else {
                    Errors.set(response.data);
                    $location.path('/error');
                }
                return $q.reject(response);
            }
        };

        /**
         * Generic GET method call
         * @param name
         * @param {Object} [params]
         * @returns {Promise}
         */
        Api.get = function(name, params, config) {
            return $http({
                method: 'GET',
                url: options.apiUrl + name,
                params: makeParams(params)
            }).then(null, getErrorHandler(config));
        };

        /**
         * Generic POST method call
         * @param name
         * @param params
         * @returns {Promise}
         */
        Api.post = function(name, params, config) {
            return $http.post(options.apiUrl + name, makeParams(params), {}).then(null, getErrorHandler(config));
        };

        /**
         * Generic DELETE method call
         * @param name
         * @param params
         * @returns {Promise}
         */
        Api.delete = function(name, params, config) {
            return $http.delete(options.apiUrl + name, {params: makeParams(params)}).then(null, getErrorHandler(config));
        };

        /**
         * Generic PUT method call
         * @param name
         * @param params
         * @returns HttpPromise
         */
        Api.put = function(name, params, config) {
            return $http.put(options.apiUrl+name, makeParams(params), {}).then(null, getErrorHandler(config));
        };

        Api.setToken = function(token) {
            window.localStorage.setItem('accessToken', token);
        };

        Api.getToken = function() {
            return window.localStorage.getItem('accessToken');
        };

        return Api;
    }];
});