'use strict';

angular.module('app.dal.fake.match', ['app.dal.entities.user'])
    .factory('MatchFakeApi', ['$q', 'MatchFakeData', 'User', function($q, MatchFakeData, User) {
        var MatchFakeApi = {};

        function randomMove(match, item) {
            var done;
            do {
                var fieldId = _.random(0, 8);
                if(match.state.fields[fieldId].item == undefined) {
                    match.state.fields[fieldId].item = item;
                    done = true;
                }
            } while(!done);
        }

        function areEqual() {
            var len = arguments.length;
            for (var i = 1; i < len; i++){
                if (arguments[i] == null || arguments[i] != arguments[i-1])
                    return false;
            }
            return true;
        }

        function checkMatchResult(match) {
            function findLine(match) {
                var fields = _.toArray(match.state.fields);

                for(var i = 0; i < 3; i++) {
                    if(areEqual(fields[i*3].item, fields[i*3 + 1].item, fields[i*3 + 2].item))
                        return fields[i*3].item;
                }

                for(i = 0; i < 3; i++) {
                    if(areEqual(fields[i].item, fields[i + 3].item, fields[i + 6].item))
                        return fields[i].item;
                }

                if(areEqual(fields[0].item, fields[4].item, fields[8].item))
                    return fields[0].item;

                if(areEqual(fields[2].item, fields[4].item, fields[6].item))
                    return fields[2].item;

                var usedFields = _.filter(fields, function(field) {
                    return field.item != null;
                });

                return usedFields.length > 7 ? -1 : null;
            }

            var item = findLine(match);

            switch (item) {
                case -1:
                    return -1;
                    break;
                case 1:
                    return _.findIndex(match.players, {item: item});
                    break;
                case 0:
                    return _.findIndex(match.players, {item: item});
                    break;
                default:
                    return null;
            }
        }

        MatchFakeApi.get = function(id) {
            return MatchFakeData.getAll().then(function(matches) {
                var match = _.find(matches, {id: id});
                return match ? match : $q.reject('404');
            });
        };

        MatchFakeApi.getAll = function() {
            return MatchFakeData.getAll();
        };

        MatchFakeApi.makeMove = function(id, field) {
            return MatchFakeData.getAll().then(function(matches) {
                var match = _.find(matches, {id: id});

                var numberOfPlayers = match.players.length;
                if(match.state.turn == numberOfPlayers - 1) {
                    match.state.turn = 0;
                } else {
                    match.state.turn++;
                }
                match.state.fields[field.id] = field;

                var matchResult = checkMatchResult(match);
                if(matchResult != null) {
                    match.winner = matchResult;
                    match.active = false;
                } else {
                    var item = field.item == 1 ? 0 : 1;
                    randomMove(match, item);
                    matchResult = checkMatchResult(match);
                    if(matchResult != null) {
                        match.winner = matchResult;
                        match.active = false;
                    }
                }

                return match;
            });
        };

        return MatchFakeApi;
    }])
    .factory('MatchFakeData', ['User', function(User) {
        var matches = [
            {
                id: 'WERD-WMKD',
                players: [ // first player is always match owner, who started the match
                    {
                        uid: 15,
                        name: "Max T.",
                        item: 1,
                        artefact: "droplet"
                    },
                    {
                        uid: 2,
                        name: "Mary Frather",
                        item: 0,
                        artefact: "plane"
                    }
                ],
                active: true, // match is still in progress
                state: {
                    turn: 0, // index of the player in array whose turn is now
                    fields: {
                        0: {item: 1},  // '0' is the key of a cell on the game field
                        2: {item: 0}
                    }
                }
            },
            {
                id: 'S23Q-WP9A',
                players: [ // first player is always match owner, who started the match
                    {
                        uid: 1,
                        name: "Sam Raften",
                        item: 1,
                        artefact: "droplet"
                    },
                    {
                        uid: 15,
                        name: "Max T.",
                        item: 0,
                        artefact: "tower"
                    }
                ],
                active: true,
                state: {
                    turn: 0,
                    fields: {
                        4: {item: 0}
                    }
                }
            }
        ];

        var MatchFakeData = {};

        MatchFakeData.getAll = function() {
            return User.me().then(function(user) {
                matches[0].players[0].uid = user.uid;
                matches[0].players[0].name = user.email;
                matches[1].players[1].uid = user.uid;
                matches[1].players[1].name = user.email;
                return matches;
            });
        };

        return MatchFakeData;
    }]);